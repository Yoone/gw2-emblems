#ifndef GD_FLIP_H
# define GD_FLIP_H

# include <gd.h>

void img_flip_vertical(gdImagePtr im);
void img_flip_horizontal(gdImagePtr im);

#endif // !GD_FLIP_H
