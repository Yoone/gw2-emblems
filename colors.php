<?php
define('API_COLORS', 'https://api.guildwars2.com/v1/colors.json');
define('COLOR_FILE', 'colors.bin');

$curl = curl_init();
curl_setopt($curl, CURLOPT_URL, API_COLORS);
curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);

$jcolors = json_decode(curl_exec($curl));

$colors = fopen(COLOR_FILE, 'wb');
foreach ($jcolors->colors as $id => $weights)
{
    $rgb = $weights->metal->rgb;
    fwrite($colors, pack('iC*', $id, $rgb[0], $rgb[1], $rgb[2]));
}
fclose($colors);
?>
