Guild Emblem Generator
======================

Introduction
------------

This is a piece of software that belongs to GW2.FR. It is used on the subdomain
`data.gw2.fr` and is available for use to the public at `db.gw2.fr/emblems`.

How-To
------

### Required packages

 - libcurl3-dev
 - libgd-dev
 - php5
 - php5-curl
 - apache2

### Building the binary

 - Production (for an Apache CGI server): `make` -> `emblem.cgi`
 - Debug (for command-line debugging): `make debug` -> `debug`

### Generating an up-to-date color file

The file `colors.bin` is required by the program in order to generate the
emblem.

The command to generate this file is: `php colors.php`

### Generating an emblem

#### Using the program

 - Production: `http://domain/<type>/<guild>[/<size>].png`
 - Debug: `./debug <type>/<guild>[/<size>]`

#### Specifics

 - `type` must be either *name* or *id*
 - `guild` is the name or id of the guild, depending on the type
 - `size` is optional, but must be between 16 and 256 if specified

#### Get the last emblem

The passed argument must be `misc/last[/<size>]`.
