#ifndef EXIT_CODES_H
# define EXIT_CODES_H

# define EXIT_MISSING_ARG           (2)
# define EXIT_URL_FORMAT            (3)
# define EXIT_WRONG_TYPE            (4)
# define EXIT_MISSING_GUILD         (5)
# define EXIT_JSON_PARSE_FAIL       (6)
# define EXIT_GUILD_NOT_FOUND       (7)
# define EXIT_IMG_LOAD              (8)
# define EXIT_OPEN_READ_LAST        (9)

#endif // !EXIT_CODES_H
