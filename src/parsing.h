#ifndef PARSING_H
# define PARSING_H

# include "context.h"

# define DELIM ("/")

# define FIRST_TOKEN(token, str)                    \
    char *token = strtok(str, DELIM);               \
    if (token == NULL)                              \
        exit_and_free(context, EXIT_URL_FORMAT);

# define NEXT_TOKEN(token)                          \
    token = strtok(NULL, DELIM);                    \
    if (token == NULL)                              \
        return

void parse_url(s_context *context, char *url);

#endif // !PARSING_H
