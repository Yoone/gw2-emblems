#ifndef JSON_H
# define JSON_H

# define JSON_NULL_CHECK(AST, NODE, ERROR)          \
    if (NODE == NULL || NODE->type == NX_JSON_NULL) \
    {                                               \
        nx_json_free(AST);                          \
        free(data);                                 \
        free(str_data);                             \
        exit_and_free(context, ERROR);              \
    }

# define API_GINFO  "https://api.guildwars2.com/v1/guild_details.json"  \
                    "?guild_%s=%s"

# define SRC_LAST   "last.json"

# ifndef _POSIX_C_SOURCE
#  define _POSIX_C_SOURCE 200809L
# endif // !_POSIX_C_SOURCE

# ifndef _SVID_SOURCE
#  define _SVID_SOURCE
# endif // !_SVID_SOURCE

# include "context.h"

void json_fetch_process(s_context *context);

#endif // !JSON_H
