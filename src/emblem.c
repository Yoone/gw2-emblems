#include "emblem.h"
#include "exit_codes.h"
#include <stdlib.h>
#include <gd.h>
#include <gd_flip.h>

static gdImagePtr load_image(const char *chunk, int id)
{
    char *name = SPRINTF_ALLOC(name, SRC_IMAGES, chunk, id);
    FILE *fp = fopen(name, "r");
    free(name);
    if (!fp)
    {
#ifdef DEBUG
        fprintf(stderr, "Could not open image "SRC_IMAGES, chunk, id);
#endif // DEBUG
        return NULL;
    }

    gdImagePtr img = gdImageCreateFromPng(fp);
    fclose(fp);

    gdImageSaveAlpha(img, 1);
    gdImageAlphaBlending(img, 1);

    return img;
}

static void img_colorize(gdImagePtr img, s_color *c)
{
    int img_x = gdImageSX(img);
    int img_y = gdImageSY(img);

    for (int x = 0; x < img_x; ++x)
    {
        for (int y = 0; y < img_y; ++y)
        {
            int a = gdTrueColorGetAlpha(gdImageGetTrueColorPixel(img, x, y));
            if (a < 127)
            {
                int color_alpha = gdImageColorAllocateAlpha(img,
                    c->r, c->g, c->b, a);
                gdImageSetPixel(img, x, y, color_alpha);
                gdImageColorDeallocate(img, color_alpha);
            }
        }
    }
}

static void img_resize(gdImagePtr *img, int old_size, int new_size)
{
    gdImagePtr new_img = gdImageCreateTrueColor(new_size, new_size);
    gdImageSaveAlpha(new_img, 1);
    gdImageAlphaBlending(new_img, 0);

    int trans = gdImageColorAllocateAlpha(new_img, 255, 255, 255, 127);
    gdImageFilledRectangle(new_img, 0, 0, new_size, new_size, trans);

    gdImageCopyResampled(new_img, *img, 0, 0, 0, 0, new_size, new_size,
        old_size, old_size);

    gdImageDestroy(*img);
    *img = new_img;
}

void print_emblem(s_context *context)
{
    context->img_back = load_image("back", context->id_back);
    context->img_forep = load_image("forep", context->id_fore);
    context->img_fores = load_image("fores", context->id_fore);

    if (context->img_back == NULL ||
        context->img_forep == NULL ||
        context->img_fores == NULL)
        exit_and_free(context, EXIT_IMG_LOAD);

    img_colorize(context->img_back, &context->col_back);
    img_colorize(context->img_forep, &context->col_forep);
    img_colorize(context->img_fores, &context->col_fores);

    gdImageCopy(context->img_forep, context->img_fores,
        0, 0, 0, 0, EM_MAXSIZE, EM_MAXSIZE);

    APPLY_FLIP(FORE_V, vertical, context->img_forep);
    APPLY_FLIP(FORE_H, horizontal, context->img_forep);
    APPLY_FLIP(BACK_V, vertical, context->img_back);
    APPLY_FLIP(BACK_H, horizontal, context->img_back);

    gdImageCopy(context->img_back, context->img_forep,
        0, 0, 0, 0, EM_MAXSIZE, EM_MAXSIZE);

    if (context->size != EM_MAXSIZE)
        img_resize(&context->img_back, EM_MAXSIZE, context->size);

#ifndef DEBUG
    gdImagePng(context->img_back, stdout);
#endif // !DEBUG
}
