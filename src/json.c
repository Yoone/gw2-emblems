#include "json.h"
#include "exit_codes.h"
#include "emblem.h"
#include <nxjson.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <curl/curl.h>
#include <sys/stat.h>

static size_t curl_callback(void *buf, size_t size, size_t nmemb, void *data)
{
    char **ptr = data;
    *ptr = strndup(buf, size * nmemb);
    return size * nmemb;
}

static char *curl_fetch(const char *url)
{
    CURL *curl;
    char *data;

    curl = curl_easy_init();

    curl_easy_setopt(curl, CURLOPT_URL, url);
    curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, curl_callback);
    curl_easy_setopt(curl, CURLOPT_WRITEDATA, &data);

    curl_easy_perform(curl);

    curl_easy_cleanup(curl);
    return data;
}

static s_color find_color(int color_id)
{
    FILE *fp = fopen(SRC_COLORS, "r");
    if (fp == NULL)
    {
#ifdef DEBUG
        fprintf(stderr, "Could not open "SRC_COLORS"\n");
#endif // DEBUG
        return COLOR_INIT(0, 0, 0);
    }

    int id;
    s_color col;
    while (fread(&id, sizeof (int), 1, fp) == 1 &&
           fread(&col, sizeof (s_color), 1, fp) == 1 &&
           id != color_id);

    if (id != color_id)
        col = COLOR_INIT(0, 0, 0);

    fclose(fp);
    return col;
}

static void json_process(s_context *context, const nx_json *em)
{
    context->id_back = nx_json_get(em, "background_id")->int_value;
    context->id_fore = nx_json_get(em, "foreground_id")->int_value;

    context->col_back = find_color(nx_json_get(em,
        "background_color_id")->int_value);
    context->col_forep = find_color(nx_json_get(em,
        "foreground_primary_color_id")->int_value);
    context->col_fores = find_color(nx_json_get(em,
        "foreground_secondary_color_id")->int_value);

    const nx_json *flags = nx_json_get(em, "flags");
    for (int i = 0; i < flags->length; ++i)
    {
        const char *flag = nx_json_item(flags, i)->text_value;
        if (flag[4] == 'B') // Background
        {
            if (flag[14] == 'H') // Horizontal
                context->flip += FLIP_BACK_H;
            else // Vertical
                context->flip += FLIP_BACK_V;
        }
        else // Foreground
        {
            if (flag[14] == 'H') // Horizontal
                context->flip += FLIP_FORE_H;
            else // Vertical
                context->flip += FLIP_FORE_V;
        }
    }
}

void json_fetch_process(s_context *context)
{
    char *data = NULL;

    if (context->type[0] == 'm') // misc/last
    {
        FILE *fp = fopen(SRC_LAST, "r");
        if (fp == NULL)
            exit_and_free(context, EXIT_OPEN_READ_LAST);

        struct stat last;
        stat(SRC_LAST, &last);
        data = malloc(last.st_size + 1);
        fread(data, last.st_size, 1, fp);
        data[last.st_size] = '\0';

        fclose(fp);
#ifdef DEBUG
        printf("\nLast JSON data:\n%s\n", data);
#endif // DEBUG
    }
    else
    {
        char *url = SPRINTF_ALLOC(url, API_GINFO, context->type,
            context->guild);
        data = curl_fetch(url);
        free(url);
#ifdef DEBUG
        printf("\nFetched JSON data:\n%s\n", data);
#endif // DEBUG
    }

    char *str_data = strdup(data);

    const nx_json *json = nx_json_parse_utf8(data);
    JSON_NULL_CHECK(json, json, EXIT_JSON_PARSE_FAIL);
    const nx_json *em_details = nx_json_get(json, "emblem");
    JSON_NULL_CHECK(json, em_details, EXIT_GUILD_NOT_FOUND);
    json_process(context, em_details);
    nx_json_free(json);

    if (context->type[0] != 'm') // !misc
    {
        FILE *fp = fopen(SRC_LAST, "w");
        if (fp != NULL) // TODO: maybe log this in case of error
        {
            fwrite(str_data, strlen(str_data), 1, fp);
            fclose(fp);
        }
    }

    free(data);
    free(str_data);
}
