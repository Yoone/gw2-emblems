#ifndef CONTEXT_H
# define CONTEXT_H

# define SPRINTF_ALLOC(DEST, PATTERN, ...) NULL;                    \
    DEST = malloc(snprintf(NULL, 0, PATTERN, ##__VA_ARGS__) + 1);   \
    sprintf(DEST, PATTERN, ##__VA_ARGS__);

# define COLOR_INIT(R, G, B) (s_color) {    \
    .r = R,                                 \
    .g = G,                                 \
    .b = B                                  \
  }

typedef struct
{
    unsigned char r;
    unsigned char g;
    unsigned char b;
} s_color;

# include <gd.h>

typedef struct
{
    /*
    ** Guild info
    */
    char *type; // id | name
    char *guild;
    /*
    ** Colors
    */
    s_color col_back;
    s_color col_forep;
    s_color col_fores;
    /*
    ** Image info
    */
    unsigned int size;
    unsigned int id_back;
    unsigned int id_fore;
    unsigned int flip;
    /*
    ** GD Images
    */
    gdImagePtr img_back;
    gdImagePtr img_forep;
    gdImagePtr img_fores;
} s_context;

s_context *context_init(void);
void context_destroy(s_context *context);
void exit_and_free(s_context *context, int code);

#endif // !CONTEXT_H
