#ifndef EMBLEM_H
# define EMBLEM_H

# ifdef DEBUG
#  define PRINT_FLAG(FLAG)              \
    if (context->flip >= FLIP_##FLAG)   \
    {                                   \
        context->flip -= FLIP_##FLAG;   \
        printf(" "#FLAG);               \
    }
# endif // DEBUG

# define APPLY_FLIP(FLAG, FUNC, IMAGE)  \
    if (context->flip >= FLIP_##FLAG)   \
    {                                   \
        context->flip -= FLIP_##FLAG;   \
        img_flip_##FUNC(IMAGE);         \
    }

# define EM_MAXSIZE         (256)
# define EM_MINSIZE         (16)

# define SRC_COLORS         "colors.bin"
# define SRC_IMAGES         "img/%s%d.png"
# define ERR_IMAGE          "error.png"

# define FLIP_BACK_H        (1)
# define FLIP_BACK_V        (2)
# define FLIP_FORE_H        (4)
# define FLIP_FORE_V        (8)

# include "context.h"

void print_emblem(s_context *context);

#endif // !EMBLEM_H
