#include "parsing.h"
#include "exit_codes.h"
#include "emblem.h"
#include <string.h>
#include <stdlib.h>

static void split_tokens(s_context *context, char *url)
{
    FIRST_TOKEN(tok, url);
    context->type = tok;

    NEXT_TOKEN(tok);
    context->guild = tok;
    for (int i = 0; context->guild[i] != '\0'; ++i)
        if (context->guild[i] == ' ')
            context->guild[i] = '+';

    NEXT_TOKEN(tok);
    context->size = atoi(tok);
}

void parse_url(s_context *context, char *url)
{
    split_tokens(context, url);

    if (context->type[0] != 'n' && context->type[0] != 'i' &&
        context->type[0] != 'm') // name, id or misc
        exit_and_free(context, EXIT_WRONG_TYPE);
    else if (context->guild == NULL)
        exit_and_free(context, EXIT_MISSING_GUILD);

    if (context->size > EM_MAXSIZE)
        context->size = EM_MAXSIZE;
    else if (context->size < EM_MINSIZE)
        context->size = EM_MINSIZE;
}
