#include "context.h"
#include "emblem.h"
#include <stdlib.h>
#include <stdio.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/stat.h>
#include <sys/sendfile.h>

s_context *context_init(void)
{
    s_context *context = malloc(sizeof (s_context));

    context->type = NULL;
    context->guild = NULL;

    context->col_back = COLOR_INIT(0, 0, 0);
    context->col_forep = COLOR_INIT(0, 0, 0);
    context->col_fores = COLOR_INIT(0, 0, 0);

    context->size = EM_MAXSIZE;
    context->id_back = 0;
    context->id_fore = 0;
    context->flip = 0;

    context->img_back = NULL;
    context->img_forep = NULL;
    context->img_fores = NULL;

    return context;
}

void context_destroy(s_context *context)
{
    if (context->img_back != NULL)
        gdImageDestroy(context->img_back);
    if (context->img_forep != NULL)
        gdImageDestroy(context->img_forep);
    if (context->img_fores != NULL)
        gdImageDestroy(context->img_fores);

    free(context);
}

void exit_and_free(s_context *context, int code)
{
    context_destroy(context);
    fflush(stdout);

    int fd = open(ERR_IMAGE, O_RDONLY);
    if (fd != -1)
    {
        struct stat im;
        stat(ERR_IMAGE, &im);
#ifndef DEBUG
        sendfile(STDOUT_FILENO, fd, NULL, im.st_size);
#endif // !DEBUG
        close(fd);
    }

    exit(code);
}
