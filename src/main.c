#include "context.h"
#include "parsing.h"
#include "json.h"
#include "exit_codes.h"
#include "emblem.h"
#include <stdio.h>
#include <stdlib.h>

#ifdef DEBUG
int main(int argc, char **argv)
{
    if (argc < 2)
    {
        fprintf(stderr, "usage: %s \"<type>/<guild>[/<size>]\"\n", argv[0]);
        exit(EXIT_MISSING_ARG);
    }
    char *url = argv[1];
#else
int main(void)
{
    printf("Content-Type: image/png\n\n");
    char *url = getenv("QUERY_STRING");
#endif // DEBUG

    s_context *context = context_init();

    parse_url(context, url);

#ifdef DEBUG
    printf("Parsed args:\n- Type: %s\n- Guild: %s\n- Size: %d\n",
        context->type, context->guild, context->size);
#endif // DEBUG

    json_fetch_process(context);

#ifdef DEBUG
    printf("\nEmblem info:\n- Back: %d (%u, %u, %u)\n- Fore: %d "
           "(%u, %u, %u), (%u, %u, %u)\n- Flags:",
        context->id_back,
        context->col_back.r, context->col_back.g, context->col_back.b,
        context->id_fore,
        context->col_forep.r, context->col_forep.g, context->col_forep.b,
        context->col_fores.r, context->col_fores.g, context->col_fores.b);
    PRINT_FLAG(FORE_V);
    PRINT_FLAG(FORE_H);
    PRINT_FLAG(BACK_V);
    PRINT_FLAG(BACK_H);
    printf("\n");
#endif // DEBUG

    print_emblem(context);

    context_destroy(context);
    return 0;
}
