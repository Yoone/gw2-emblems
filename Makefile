CC = gcc
CFLAGS = -std=c99 -Wall -Werror -Wextra -pedantic
INC = -I./lib
LDLIBS = -lcurl -lgd
DEBUG = -g3 -D DEBUG
PROD = -O2
AOUT = emblem.cgi
DOUT = debug

SRC = main.c context.c parsing.c json.c emblem.c
LIB = nxjson.c gd_flip.c

SRCDIR = src/
LIBDIR = lib/
BLDDIR = build/
OBJ := $(addprefix $(BLDDIR), $(SRC:.c=.o)) \
       $(addprefix $(BLDDIR), $(LIB:.c=.o))
SRC := $(addprefix $(SRCDIR), $(SRC))
LIB := $(addprefix $(LIBDIR), $(LIB))

all: CFLAGS += $(PROD)
all: $(AOUT)

$(AOUT): $(OBJ)
	$(CC) -o $@ $^ $(LDLIBS)

$(BLDDIR):
	mkdir -p $(BLDDIR)

$(BLDDIR)%.o: $(SRCDIR)%.c | $(BLDDIR)
	$(CC) $(CFLAGS) $(INC) -c $^ -o $@

$(BLDDIR)%.o: $(LIBDIR)%.c | $(BLDDIR)
	$(CC) $(CFLAGS) -c $^ -o $@

$(DOUT): CFLAGS += $(DEBUG)
$(DOUT): $(OBJ)
	$(CC) -o $@ $^ $(LDLIBS)

clean:
	$(RM) $(OBJ)
	$(RM) $(AOUT)
	$(RM) $(DOUT)

distclean: clean
	$(RM) -r $(BLDDIR)
	$(RM) colors.bin
	$(RM) last.json

.PHONY: clean distclean
